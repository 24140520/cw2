
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/css/app.css"/>
    <title>{{ $survey->title }}</title>
</head>
<body>
<h1>{{ $survey->title }}</h1>
<p>{{$survey->description}}</p>
<!-- <section>
        @if (isset ($questions))
            <ul>
                @foreach ($questions as $question)
                    <li><a href="/admin/questions/{{ $question->id }}" name="{{ $question->title }}">{{ $question->title }}</a></li>
                @endforeach
            </ul>
        @else
            <p>no questions yet</p>
        @endif
    </section> -->

    {{ Form::open(array('action' => 'QuestionController@create', 'method' => 'get')) }}

    {{ csrf_field() }}

    <div class="row">
        {!! Form::submit('Add Question', ['class' => 'button']) !!}
    </div>
    
    {{ Form::close() }}
    
</body>
</html>