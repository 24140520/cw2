<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/css/app.css"/>

    <title>Create Survey</title>
</head>
<body>
<h1>Create new survey</h1>

{{ Form::open(array('action' => 'SurveyController@store', 'class' => 'createsurvey')) }}

    {{ csrf_field() }}

    {!! Form::hidden ('creator_id', Auth::Id()) !!}


    <div class="row large-12 columns">
        {!! Form::label('title', 'Title:') !!}
        {!! Form::text('title', null, ['class' => 'large-8 columns']) !!}
    </div>

    <div class="row large-12 columns">
        {!! Form::label('description', 'Description:') !!}
        {!! Form::text('description', null, ['class' => 'large-8 columns']) !!}
    </div>

    <div class="row">
        {!! Form::submit('Create Survey', ['class' => 'button']) !!}
    </div>
    {{ Form::close() }}


    
</body>
</html>