<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/css/app.css"/>
    <title>User Dashboard</title>
</head>
<body>
<h1>Surveys</h1>

{{ Form::open((array('action' => 'SurveyController@create', 'method' => 'get'))) }}

{{ csrf_field() }}
    <div class="row">
        {!! Form::submit('Create new survey', ['class' => 'button']) !!}
    </div>
    {{ Form::close() }}


<section>
        @if (isset ($surveys))
            <ul>
                @foreach ($surveys as $survey)
                    <li><a href="/admin/surveys/show/{{ $survey->id }}" name="{{ $survey->title }}">{{ $survey->title }}</a></li>
                @endforeach
            </ul>
        @else
            <p>no surveys created yet</p>
        @endif
    </section>

</body>
</html>

