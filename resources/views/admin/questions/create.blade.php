<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/css/app.css"/>
    <title>new question</title>
</head>
<body>

<h1>new question</h1>

{{ Form::open(array('action' => 'QuestionController@store', 'class' => 'createquestion')) }}

{{ csrf_field() }}

    <div class="row large-12 columns">
        {!! Form::hidden('survey_id', $surveys->id, ['class' => 'large-8 columns'])!!}
    </div>

    <div class="row large-12 columns">
        {!! Form::label('question_title', 'Title:') !!}
        {!! Form::text('question_title', null, ['class' => 'large-8 columns']) !!}
    </div>

    <div class="row large-12 columns">
        {!! Form::label('response_a', 'Response A:') !!}
        {!! Form::text('response_a', null, ['class' => 'large-8 columns']) !!}
    </div>

    <div class="row large-12 columns">
        {!! Form::label('response_b', 'Response B:') !!}
        {!! Form::text('response_b', null, ['class' => 'large-8 columns']) !!}
    </div>

    <div class="row large-12 columns">
        {!! Form::label('response_c', 'Response C:') !!}
        {!! Form::text('response_c', null, ['class' => 'large-8 columns']) !!}
    </div>

    <div class="row large-12 columns">
        {!! Form::label('response_d', 'Response D:') !!}
        {!! Form::text('response_d', null, ['class' => 'large-8 columns']) !!}
    </div>

    <div class="row">
        {!! Form::submit('Add Question', ['class' => 'button']) !!}
    </div>
    {{ Form::close() }}


    
</body>
</html>