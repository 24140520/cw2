<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/css/app.css"/>

    <title>complete a survey</title>
</head>
<body>
<h1>Choose a survey to complete</h1>

<section>
        @if (isset ($surveys))
            <ul>
                @foreach ($surveys as $survey)
                    <li><a href="complete/{{ $survey->id }}" name="{{ $survey->title }}">{{ $survey->title }}</a></li>
                @endforeach
            </ul>
        @else
            <p>no surveys created yet</p>
        @endif
    </section>
    
</body>
</html>