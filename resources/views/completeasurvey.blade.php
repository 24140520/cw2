<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/css/app.css"/>

    <title>complete a survey</title>
</head>
<body>
<h1>complete</h1>
{{ Form::open(array('action' => 'HomeController@store', 'class' => 'surveyresponse')) }}

    {{ csrf_field() }}

    {!! Form::text ('question_id', '9') !!}


    <div class="row large-12 columns">
        {!! Form::label('response', 'Response:') !!}
        {!! Form::text('response', ['class' => 'large-8 columns']) !!}
    </div>

    <div class="row">
        {!! Form::submit('Submit Answer', ['class' => 'button']) !!}
    </div>
    {{ Form::close() }}

</body>
</html>