<?php

use Illuminate\Database\Seeder;

class SurveyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('surveys')->insert([
            ['id' => 1, 'creator_id' => '2', 'title' => "survey 1", 'description' => 'survey description'],
            ['id' => 2, 'creator_id' => '2', 'title' => "survey 2", 'description' => 'survey description'],
            ['id' => 3, 'creator_id' => '2', 'title' => "survey 3", 'description' => 'survey description'],
            ['id' => 4, 'creator_id' => '2', 'title' => "survey 4", 'description' => 'survey description']
        //    ['id' => 1, 'title' => "survey 1", 'description' => 'survey description'],
        //     ['id' => 2, 'title' => "survey 2", 'description' => 'survey description'],
        //     ['id' => 3, 'title' => "survey 3", 'description' => 'survey description'],
        //     ['id' => 4, 'title' => "survey 4", 'description' => 'survey description']
        ]);
    }
    
}
