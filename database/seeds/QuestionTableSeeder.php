<?php

use Illuminate\Database\Seeder;

class QuestionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('questions')->insert([
            ['id' => 1, 'survey_id' => '1', 'question_title' => "how many planets are there in our solar system", 'response_a' => "4", 'response_b' => "6", 'response_c' => "7", 'response_d' => "8"],
            ['id' => 2, 'survey_id' => '1', 'question_title' => "question 2", 'response_a' => "an answer", 'response_b' => "another answer", 'response_c' => "incorrect answer", 'response_d' => "correct answer"],
            ['id' => 3, 'survey_id' => '1', 'question_title' => "question 3", 'response_a' => "answer 1", 'response_b' => "answer 2", 'response_c' => "answer 3", 'response_d' => "answer 4"],
            ['id' => 4, 'survey_id' => '1', 'question_title' => "question 4", 'response_a' => "an answer", 'response_b' => "another answer", 'response_c' => "incorrect answer", 'response_d' => "correct answer"]
            // ['id' => 1, 'question_title' => "how many planets are there in our solar system", 'response_a' => "4", 'response_b' => "6", 'response_c' => "7", 'response_d' => "8"],
            // ['id' => 2, 'question_title' => "question 2", 'response_a' => "an answer", 'response_b' => "another answer", 'response_c' => "incorrect answer", 'response_d' => "correct answer"],
            // ['id' => 3, 'question_title' => "question 3", 'response_a' => "answer 1", 'response_b' => "answer 2", 'response_c' => "answer 3", 'response_d' => "answer 4"],
            // ['id' => 4, 'question_title' => "question 4", 'response_a' => "an answer", 'response_b' => "another answer", 'response_c' => "incorrect answer", 'response_d' => "correct answer"]
        ]);

    }
}
