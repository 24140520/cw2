<?php

use Illuminate\Database\Seeder;

class ResponseTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('responses')->insert([
            ['id' => 1, 'question_id' => '1', 'response' => "1"],
            ['id' => 2, 'question_id' => '1', 'response' => "2"],
            ['id' => 3, 'question_id' => '1', 'response' => "3"],
            ['id' => 4, 'question_id' => '1', 'response' => "4"]
        ]);
    }
}
