<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Survey;
use App\Question;
use App\Response;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        $this->call(UserTableSeeder::class);
        $this->call(SurveyTableSeeder::class);
        $this->call(QuestionTableSeeder::class);
        $this->call(ResponseTableSeeder::class);



         /*
        * run factories
        */

        factory(User::class, 50)->create();
        factory(Survey::class, 5)->create();
        factory(Question::class, 20)->create();
        factory(Response::class, 60)->create();


        // $this->call(UsersTableSeeder::class);

        // following code is to seed user db table with fake data to test
        // disable foreign key check for this connection before running seeders
        // DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        // // Model::unguard();

        //   User::truncate();

        //   //re-enable foreign key check for this connection
        //   DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        //   factory(User::class, 50)->create();

        // //   Model::reguard();



    }
}
