<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\User;
use App\Survey;
use App\Question;
use App\Response;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        // 'email_verified_at' => now(),
        // 'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        'password' => bcrypt(Str::random(10)),
        'remember_token' => Str::random(10),
    ];
});


$factory->define(Survey::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence,
        'description' => $faker->sentence,
        'creator_id' => '420',
    ];
});

$factory->define(Question::class, function (Faker $faker) {
    return [
        'survey_id' => '1',
        'question_title' => $faker->sentence,
        'response_a' => $faker->sentence,
        'response_b' => $faker->sentence,
        'response_c' => $faker->sentence,
        'response_d' => $faker->sentence,
    ];
});

$factory->define(Response::class, function (Faker $faker) {
    return [
        'question_id' => '1',
        'response' => $faker->sentence,
    ];
});


