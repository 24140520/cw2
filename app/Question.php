<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{

    protected $fillable = [
        'question_title',
        'survey_id',
        'response_a',
        'response_b',
        'response_c',
        'response_d'
    ];

    /**
     * Get the survey associated with the question
     *
     * @return mixed
     */
    public function survey()
    {
        return $this->belongsTo('App\Survey');
    }

    /**
     * Get the responses associated with the question.
     *
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function response()
    {
        return $this->hasMany('App\Response');
    }
}
