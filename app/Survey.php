<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Survey extends Model
{
    protected $fillable = [
        'creator_id',
        'title',
        'description'
    ];

    /**
     * Get the user that created the survey
     *
     * @return mixed
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function question() {

        return $this->hasMany('App\Questions');
    }
}
