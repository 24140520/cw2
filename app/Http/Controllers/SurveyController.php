<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Survey;
Use App\User;
Use App\Question;

class SurveyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //get all the surveys
        $surveys = Survey::all();

        return view('admin/surveys', ['surveys' =>$surveys]);
    }

    /**
     * Show the form for creating a new survey.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){

        // //return the create surveys view 
        return view('admin/surveys/create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $surveys = Survey::create($request->all());

        return view ('admin/questions/create', compact('surveys'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // get the survey
        $survey = Survey::where('id',$id)->first();

        // if survey does not exist return to list
        if(!$survey)
        {
            return redirect('/admin/surveys');
        }
        return view('/admin/surveys/show')->withSurvey($survey);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
