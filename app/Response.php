<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Response extends Model
{
    protected $fillable = [
        'response',
        'question_id'
    ];

    /**
     * Get the question associated with the response
     *
     * @return mixed
     */
    public function question()
    {
        return $this->belongsTo('App\Question');
    }
}
