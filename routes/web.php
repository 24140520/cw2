<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//displaying the home/login & signup page 
Route::get('/', function () {
    return view('auth.login');
});


// Route::resource('/surveys', 'HomeController');
Route::resource('/completeasurvey', 'HomeController');



/*
/Application routes
/ this route group applies to the 'web' middleware group to every route it contains. 
/the 'web' middleware group is defined in your http kernel and includes session state
/csrf protection and more.
*/

/**
 * any pages that require a user to login route through the auth middleware
 */

Route::group(['middleware' => 'web'], function () {
    Route::auth();

    //this route is for the survey pages
    Route::resource('/admin/surveys', 'SurveyController');

    //this route is for the question pages
    Route::resource('/admin/questions', 'QuestionController');
});
