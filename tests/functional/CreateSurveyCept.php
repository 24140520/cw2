<?php 
$I = new FunctionalTester($scenario);

$I ->am('admin');
$I->wantTo('Create a new survey');

//login as user
//id should be 1 as this can also be manually tested with known information
Auth::loginUsingId(1);

// add a test survey to check that content can be seen
$I->haveRecord('surveys', [
    'id' => '42',
    'title' => 'test survey 1',
    'description' => 'test survey',
    'creator_id' => '1',
]);


// create an article linked to one category
// When
$I->amOnPage('/admin/surveys');
$I->see('Surveys', 'h1');
$I->see('test survey 1');
// And
$I->click('Create new survey');
//then
$I->amOnPage('/admin/surveys/create');
//and
$I->see('Create new survey', 'h1');
//then
$I->submitForm('.createsurvey', [
    'id' => '90',
    'title' => 'test survey 2',
    'description' => 'test survey',
    'creator_id' => '1',
]);
//then
$I->amOnPage('/admin/questions/create');
//and
$I->see('h1', 'new question');
//then
$I->submitForm('.createquestion', [
    'title' => 'what is 4 * 4',
    'response_a' => '8',
    'response_b' => '12',
    'response_c' => '16',
    'response_d' => '20',
    'survey_id' => '90'
]);

//then
$I->amOnPage('/admin/surveys/create');
