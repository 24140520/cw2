<?php 
$I = new FunctionalTester($scenario);

$I->am('unregistered user');
$I->wantTo('sign up and create an account');

//when
$I->amOnPage('/');
$I->see('Login or create account', 'h3');

//and
$I->click('Register');

//then
$I->amOnPage('/register');
//and
$I->see('Create Account', 'h1');
$I->submitForm('.createaccount', [
    'name' => 'Ste Coleman',
    'email' => '24140520@edgehill.ac.uk',
    'password' => 'password',
    'password' => 'password'
]);
//then
$I->amOnPage('/register');
//and
$I->submitForm('.createaccount', [
    'email' => '24140520@edgehill.ac.uk',
    'password' => 'password',
]);
//then
$I->amOnPage('/admin/surveys');