<?php 
$I = new FunctionalTester($scenario);

$I ->am('admin');
$I->wantTo('Add a question to a siurvey');

//login as user
//id should be 1 as this can also be manually tested with known information
Auth::loginUsingId(1);

// add a test survey to check that content can be seen
$I->haveRecord('surveys', [
    'id' => '42',
    'title' => 'test survey 1',
    'description' => 'test survey',
    'creator_id' => '1',
]);


// create an article linked to one category
// When
$I->amOnPage('/admin/surveys');
$I->see('Surveys', 'h1');
$I->see('test survey 1');
// And
$I->click('test survey 1');
//then
$I->amOnPage('/admin/questions/create');
//and
$I->see('h1', 'new question');
//then
$I->submitForm('.createquestion', [
    'title' => 'what is 6 * 6',
    'response_a' => '8',
    'response_b' => '12',
    'response_c' => '16',
    'response_d' => '20',
    'survey_id' => '90'
]);

//then
$I->amOnPage('/admin/surveys/show/42');
